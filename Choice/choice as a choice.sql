SELECT			pol.policy_num,
				p.product_name,
				mqs.calendar_date_key,
				pol.policy_term_effective_dt,
				CASE WHEN orig.originalquotedate IS NOT NULL THEN 'Pre' ELSE 'Post' END AS timingflag,
				agt.agent_cd,
				CASE
					WHEN pt.ssa > 0 THEN CASE
						WHEN pt.choice = 0 THEN 'SSA Only'
						WHEN pt.choice > 0 AND fp.first_prod = 'Auto Signature Series' THEN 'Both: SSA First'
						ElSE 'Both: Choice First' END
					WHEN pt.choice > 0 THEN 'Choice Only'
					ELSE 'Unknown Auto' END AS ProdSelection,
				mqs.FULL_TERM_PREMIUM_AMT,
				t.policy_tier_cd,
				au.coverage_type_desc,
				au.num_of_vehicles,
				CASE WHEN INSTR(UPPER(qu.quote_customer_key),'TEST') = 0 THEN 'N' ELSE 'Y' END AS testflag,
				mqs.unique_quote_count,
				mqs.bound_count
FROM			bdw_dal.f_monthly_quote_snapshot mqs
	JOIN		bdw_dal.d_state s
		ON		mqs.policy_state_key = s.state_key
	JOIN		bdw_dal.d_product p
		ON		mqs.product_key = p.product_key
	JOIN		bdw_dal.d_policy pol
		ON		mqs.policy_key = pol.policy_key
	JOIN		bdw_dal.d_auto_policy au
		ON		mqs.auto_policy_key = au.auto_policy_key
	JOIN		bdw_dal.d_quote_unique qu
		ON		mqs.quote_unique_key = qu.quote_unique_key
	JOIN		bdw_dal.d_agent agt
		ON		mqs.agent_selling_key = agt.agent_key
	JOIN		bdw_dal.d_policy_tier t
		ON		mqs.policy_tier_key = t.policy_tier_key
	JOIN
	(
				SELECT			qu.quote_customer_key,
								MIN(fpt.transaction_key) AS first_trans
				FROM			bdw_dal.f_policy_transaction fpt
					JOIN		bdw_dal.d_quote_unique qu
						ON		fpt.quote_unique_key = qu.quote_unique_key
					JOIN		bdw_dal.d_transaction_type tt
						ON		fpt.transaction_type_key = tt.transaction_type_key
				WHERE			fpt.transaction_entry_dt_key BETWEEN &fromDateKey AND &toDateKey
					AND			tt.transaction_type_desc IN('quote','policy')
				GROUP BY		qu.quote_customer_key
	) fd
		ON		qu.quote_customer_key = fd.quote_customer_key
	LEFT JOIN
	(
				SELECT			qu.quote_customer_key,
								MIN(fpt.transaction_key) AS originalquotedate
				FROM			bdw_dal.f_policy_transaction fpt
					JOIN		bdw_dal.d_quote_unique qu
						ON		fpt.quote_unique_key = qu.quote_unique_key
					JOIN		bdw_dal.d_transaction_type tt
						ON		fpt.transaction_type_key = tt.transaction_type_key
				WHERE			fpt.transaction_entry_dt_key < &fromDateKey
					AND			tt.transaction_type_desc IN('quote','policy')
				GROUP BY		qu.quote_customer_key
	) orig
		ON		qu.quote_customer_key = orig.quote_customer_key
	JOIN
	(
				SELECT DISTINCT	fpt.transaction_key,
								p.product_name AS first_prod
				FROM			bdw_dal.f_policy_transaction fpt
					JOIN		bdw_dal.d_product p
						ON		fpt.product_key = p.product_key
	) fp
		ON		fd.first_trans = fp.transaction_key
	JOIN
	(
				SELECT			qu.quote_customer_key,
								SUM(CASE WHEN p.product_name = 'Auto Signature Series' THEN 1 ELSE 0 END) AS ssa,
								SUM(CASE WHEN p.product_name = 'Choice Auto' THEN 1 ELSE 0 END) AS choice
				FROM			bdw_dal.f_policy_transaction fpt
					JOIN		bdw_dal.d_quote_unique qu
						ON		fpt.quote_unique_key = qu.quote_unique_key
					JOIN		bdw_dal.d_product p
						ON		fpt.product_key = p.product_key
					JOIN		bdw_dal.d_transaction_type tt
						ON		fpt.transaction_type_key = tt.transaction_type_key
				WHERE			fpt.transaction_entry_dt_key BETWEEN &fromDateKey AND &toDateKey
					AND			tt.transaction_type_desc IN('quote','policy')
				GROUP BY		qu.quote_customer_key
	) pt
		ON		qu.quote_customer_key = pt.quote_customer_key
WHERE			s.state_cd = &reportState
	AND			p.line_of_business_name = 'Auto'
	AND			mqs.calendar_date_key BETWEEN &fromDateKey AND &toDateKey
	AND			TRUNC(pol.policy_term_effective_dt) >= &oracleFirstEffDate
	AND			mqs.close_ratio_type_key = 30
	AND			UPPER(pol.import_type_desc) IN('MISSING_VALUE','NEW','NEW BUSINESS','SPIN','SPIN-OFF','SPLIT')
